import React, { useState, useContext } from 'react'
import PassingDataContext from '../../Context/dataPassing';
import Tweet from './Tweet/Tweet';
import './Tweets.css'

const Tweets = props => {

    const context = useContext(PassingDataContext)
    const [showTweet, setShowTweet] = useState({})

    const showTweetHandler = (value) => {
        setShowTweet({
            [value]: true
        })
    }


    return (
        <div className="tweets-main">
            <header className="tweets-header">
                <h2>Tweets of Donald trump</h2>
                <p>Below are some of the tweets of donald trump which he had his views upon.</p>
            </header>
            <main>
                {context.tweets[0]._embedded.tag.map((tweet, index) => (
                    <div className="tweet-name" key={tweet.value}>
                        <div className="tweet-name-details">
                            <p className="tweet-name-value">{tweet.value}</p>
                            <p className="tweet-see" onClick={() => showTweetHandler(tweet.value)}>Show Tweets</p>
                        </div>
                        {showTweet[tweet.value] && <Tweet tag={tweet.value} />}
                    </div>
                ))}
            </main>
        </div>
    );
}

export default Tweets